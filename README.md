# Yaltai-Fork

Fork du dataset 
Clérice, T. (2022). YALTAi: Segmonto Manuscript and Early Printed Book Dataset (1.0.0) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.6814770

avec ajout d'images relatif aux cartes.

Création du modèle crée avec une carte graphique NVIDIA GeForce RTX 2060 SUPER, 7974MiB.
Configuration logiciel :
Ultralytics YOLOv8.0.200 🚀 Python-3.11.2 torch-2.1.0+cu121

Lancement de la commande de création du modèle :
bin/yolo detect train data=Yolo-DataSet/config.yml model=yolov8n.pt epochs=100 imgsz=640
Sortie : 100 epochs completed in 0.964 hours.

Lancement de la commande pour analyser les images avec création du fichier Json :
python3 Yolo-Detect.py --path Data/httpsapiblukmetadataiiifark81055vdc_1000560034830x000001manifestjson

Lancement de la commande pour découper les images :
python3 Yolo-ImgExtract.py

