from ultralytics import YOLO
import argparse
import json
import logging
import os

def detectionYolo(path):
    # Load a pretrained YOLOv8n model
    model = YOLO('../Model/Yolo-Yaltai-Fork.pt')

    # Run inference on the source
    results = model(path, stream=True)  # generator of Results objects
    # Process results generator
    i = 0
    dictResult = {}
    for result in results:
        for element in json.loads(result.tojson()):
            element["path"] = result.path
            i = i + 1
            dictResult[i] = element
    yoloFile = os.path.join(path, "yolo.json")
    with open(yoloFile, "w") as outfile: 
        json.dump(dictResult, outfile)

def main():
    """ Main method """
    # Parse arguments
    parser = argparse.ArgumentParser(description='Yolo detection')
    parser.add_argument('--path', type=str, required=True, help='Base folder')

    args = parser.parse_args()
    """configuration des logs"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    yoloFile = os.path.join(args.path, "yolo.json")
    logging.info("Démarrage de Yolo detection")
    if not os.path.isfile(yoloFile):
        detectionYolo(args.path)

if __name__== "__main__":
    main()
