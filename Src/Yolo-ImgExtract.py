import json
import logging
import os
from PIL import Image
import requests
from io import BytesIO
import csv
import time
import cv2

def getMetadata(path, writer, imageName, url1):
    '''recupere les positions'''
    newPath = os.path.join(path, "data.json")
    with open(newPath, "r") as dataFile:
        metadata = json.load(dataFile)
        titre = metadata["Title"]
        titre = titre.replace("\n"," ")
        try:
            date = metadata["Date"]
        except:
            date = ""
        try:
            related = metadata["Related"]
        except:
            related = ""
        try:
            biblissima = metadata["Biblissima URL"]
        except:
            biblissima = ""
        writer.writerow([imageName, titre, metadata["Source"], metadata["Library"], metadata["Shelfmark"], date, metadata["Manifest URL"], biblissima, related, url1])
    return writer

def getdictImages(path):
    '''recupere les positions'''
    newPath = os.path.join(path, "data.json")
    with open(newPath, "r") as dataFile:
        data = json.load(dataFile)
        dictImages = data["images"]
    return dictImages

def getPosition(path, writer):
    '''recupere les positions'''
    dictImages = getdictImages(path)
    newPath = os.path.join(path, "yolo.json")
    if os.path.isfile(newPath):
        with open(newPath, "r") as yoloFile:
            i = 1
            try:
                data = json.load(yoloFile)
            except:
                logging.error("Erreur de chargement du fichier json %s" % (yoloFile))
            else:
                for image in data.values():
                    if image["name"] == "GraphicZone":
                        try:
                            img = cv2.imread(image["path"])
                        except cv2.error as e:
                            logging.error("Erreur de chargement de Image %s" % (e))
                        else:
                            x = int(image["box"]["x1"])
                            w = int(image["box"]["x2"] - image["box"]["x1"])
                            y = int(image["box"]["y1"])
                            h = int(image["box"]["y2"] - image["box"]["y1"])
                            crop_img = img[y:y+h, x:x+w]
                            imageName = image["path"].split("/")[-2] + "-" + str(i) + "-" + image["path"].split("/")[-1]
                            fileName = os.path.join("Yolo-Resultats/", imageName)
                            if not os.path.isfile(fileName):
                                try:
                                    cv2.imwrite(fileName, crop_img)
                                except cv2.error as e:
                                    logging.error("Erreur de Ecriture de Image %s" % (e))
                                else:
                                    logging.info("Ecriture image %s" % (imageName))
        return writer

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    header = ["Image","Titre","Source","Bibliothèque", "Côte", "Date","Manifest Url","Biblissima URL", "Related", "URL"]
    try:
        fichier = open('Yolo-Resultats/Listing-Data.csv', 'a', encoding='UTF8')
    except IOError:
        logging.error("Error Ouverture Fichier CSV")
    writer = csv.writer(fichier, dialect='excel')

    Path = "Data/"
    dirRepertoire = [name for name in os.listdir(Path) if os.path.isdir(os.path.join(Path, name))]
    for Repertoire in dirRepertoire:
        newPath = os.path.join(Path , Repertoire)
        writer = getPosition(newPath, writer)

if __name__ == "__main__":
        main()

